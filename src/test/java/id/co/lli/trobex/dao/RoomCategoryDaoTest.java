/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao;

import id.co.lli.trobex.dao.RoomCategoryDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author kris
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context.xml"})
public class RoomCategoryDaoTest {
    
    @Autowired
    private RoomCategoryDao roomCategoryDao;
    
    @Test
    public void getByDistantKey() {
        System.out.println(roomCategoryDao.getByDistantKey("01"));
    }
}
