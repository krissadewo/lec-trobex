/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao;

import id.co.lli.trobex.dao.PurchaseItemDao;
import id.co.lli.trobex.entity.trobex.TPostItem;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author kris
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context.xml"})
public class PurchaseItemDaoTest {
    
    @Autowired
    private PurchaseItemDao purchaseItemDao;
    
    @Test
    public void insert(){
        List<TPostItem> postItems = new ArrayList<>();
        TPostItem item = new TPostItem();
        item.setCurrency("abc");
        postItems.add(item);
        purchaseItemDao.createPostItemData(postItems);
    }
}
