/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao;


import id.co.lli.trobex.dao.TrobexDao;
import id.co.lli.trobex.App;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author kris
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context.xml"})
public class TrobexDaoTest {

    @Autowired
    private TrobexDao trobexDao;

    @Test
    public void getByJobKey() {
        System.out.println(trobexDao.getByJobKey(App.JOB_KEY_INHOUSE));
    }
}
