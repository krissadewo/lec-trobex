CREATE TABLE `trobex` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `distant_key` varchar(255) DEFAULT NULL,
  `vstamp` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `service_url` varchar(255) NOT NULL,
  `time_interval` int(11) NOT NULL,
  `job_key` varchar(255) NOT NULL,
  `api_key` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8