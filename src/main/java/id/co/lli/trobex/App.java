/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex;

/**
 *
 * @author kris
 */
public class App {

    public static final String JOB_KEY_INHOUSE = "IH";
    public static final String JOB_KEY_INHOUSE_BY_ROOM = "IHR";
    public static final String JOB_KEY_PURCHASE_ITEM = "PI";  
    public static final String JOB_KEY_ROOM = "RO";
    public static final String JOB_KEY_POST_USAGE_ADMISSION = "PU";
    public static final String JOB_KEY_POST_RESTAURANT = "PR";
    public static final String KEY_DATA = "data";
    public static final String KEY_STATUS = "status";
    public static final String EXPECTED_RESULT = "200";
}
