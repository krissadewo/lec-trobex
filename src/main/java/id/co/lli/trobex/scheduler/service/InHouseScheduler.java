/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.scheduler.service;

import id.co.lli.trobex.App;
import id.co.lli.trobex.scheduler.LecScheduler;
import id.co.lli.trobex.scheduler.job.InHouseJob;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.springframework.stereotype.Component;

/**
 *
 * @author kris
 */
@Component
public class InHouseScheduler extends LecScheduler {

    @Override
    public void process() {
        JobDetail jobDetail = JobBuilder.newJob(InHouseJob.class).withIdentity(App.JOB_KEY_INHOUSE).build();
        SimpleTrigger simpleTrigger = TriggerBuilder.newTrigger().withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(getTrobex().getInterval()).repeatForever()).build();
        jobDetail.getJobDataMap().put("trobex", getTrobex());
        jobDetail.getJobDataMap().put("jsonParser", getLecJsonParser());
        jobDetail.getJobDataMap().put("trobexService", getTrobexService());
        start(jobDetail, simpleTrigger);
    }

    @Override
    protected JobKey getJobKey() {
        return new JobKey(App.JOB_KEY_INHOUSE);
    }
}
