/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.scheduler.job;

import id.co.lli.trobex.App;
import id.co.lli.trobex.entity.trobex.TCustomer;
import id.co.lli.trobex.entity.trobex.TCustomerList;
import id.co.lli.trobex.entity.trobex.Trobex;
import id.co.lli.trobex.helper.LecJsonParser;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import id.co.lli.trobex.dao.service.TrobexService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author kris
 */
@DisallowConcurrentExecution
public class InHouseJob implements Job {

    private static Logger logger = Logger.getLogger(InHouseJob.class);

    @Autowired
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap data = context.getJobDetail().getJobDataMap();
        Trobex trobex = (Trobex) data.get("trobex");
        TrobexService trobexService = (TrobexService) data.get("trobexService");
        LecJsonParser lecJsonParser = (LecJsonParser) data.get("jsonParser");
        JsonObject jsonObject = lecJsonParser.getJsonObject(trobex.getServiceUrl(), trobexService.getTrobexApiKey().getValue());
        TCustomerList customerList = lecJsonParser.getGson().fromJson(jsonObject, TCustomerList.class);
        if (customerList.getStatus().equals(App.EXPECTED_RESULT)) {
            List<TCustomer> guests = lecJsonParser.getGson().fromJson(jsonObject.getAsJsonArray(App.KEY_DATA), new TypeToken<List<TCustomer>>() {
            }.getType());
            customerList.setCustomers(guests);
            SimpleDateFormat dateFormat = new SimpleDateFormat("H:ss");
            logger.info("inhouse job executed every " + trobex.getInterval() + "  second");
            logger.info(dateFormat.format(new Date()) + "-" + customerList.toString());
        } else {
            logger.error("Unexpected result : " + customerList.getStatus());
        }
    }
}
