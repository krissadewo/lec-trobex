/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.scheduler;

import id.co.lli.trobex.dao.service.TrobexService;
import id.co.lli.trobex.entity.trobex.Trobex;
import id.co.lli.trobex.helper.LecJsonParser;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author kris
 */
@Component
public abstract class LecScheduler {

    @Autowired
    private TrobexService trobexService;
    @Autowired
    private LecJsonParser lecJsonParser;
    private Scheduler scheduler;
    private static Logger logger = Logger.getLogger(LecScheduler.class);

    @PostConstruct
    public void init() {
        try {
            scheduler = new StdSchedulerFactory().getScheduler();
        } catch (SchedulerException ex) {
        }
    }

    public Trobex getTrobex() {
        return trobexService.getByJobKey(getJobKey().getName());
    }

    public TrobexService getTrobexService() {
        return trobexService;
    }

    public LecJsonParser getLecJsonParser() {
        return lecJsonParser;
    }

    abstract public void process();

    abstract protected JobKey getJobKey();

    public synchronized void start(JobDetail jobDetail, SimpleTrigger simpleTrigger) {
        try {
            if (scheduler.checkExists(getJobKey())) {
                this.reset();
            }

            scheduler.scheduleJob(jobDetail, simpleTrigger);

            if (!scheduler.isStarted()) {
                scheduler.start();
            }
        } catch (SchedulerException ex) {
            logger.error(ex.getMessage());
        }
    }

    public synchronized void reset() {
        try {
            if (scheduler.isStarted()) {
                scheduler.deleteJob(getJobKey());
            }
        } catch (SchedulerException ex) {
            logger.error(ex.getMessage());
        }
    }

    public synchronized void pause() {
        try {
            if (scheduler.isStarted()) {
                scheduler.pauseJob(getJobKey());
            }
        } catch (SchedulerException ex) {
            logger.error(ex.getMessage());
        }
    }

    public synchronized void resume() {
        try {
            if (scheduler.isStarted()) {
                scheduler.resumeJob(getJobKey());
            }
        } catch (SchedulerException ex) {
            logger.error(ex.getMessage());
        }
    }
}
