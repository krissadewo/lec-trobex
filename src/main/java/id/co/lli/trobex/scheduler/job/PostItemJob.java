/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.scheduler.job;

import id.co.lli.trobex.App;
import id.co.lli.trobex.dao.service.TrobexService;
import id.co.lli.trobex.entity.trobex.TPostItem;
import id.co.lli.trobex.entity.trobex.TPostItemList;
import id.co.lli.trobex.entity.trobex.Trobex;
import id.co.lli.trobex.helper.LecJsonParser;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author kris
 */
@DisallowConcurrentExecution
public class PostItemJob implements Job {

    private static Logger logger = Logger.getLogger(PostItemJob.class);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap data = context.getJobDetail().getJobDataMap();
        Trobex trobex = (Trobex) data.get("trobex");
        LecJsonParser lecJsonParser = (LecJsonParser) data.get("jsonParser");
        TrobexService trobexService = (TrobexService) data.get("trobexService");
        JsonObject jsonObject = lecJsonParser.getJsonObject(trobex.getServiceUrl(), trobexService.getTrobexApiKey().getValue());

        TPostItemList postItemList = lecJsonParser.getGson().fromJson(jsonObject, TPostItemList.class);
        if (postItemList.getStatus().equals(App.EXPECTED_RESULT)) {
            List<TPostItem> postItems = lecJsonParser.getGson().fromJson(jsonObject.getAsJsonArray(App.KEY_DATA), new TypeToken<List<TPostItem>>() {
            }.getType());
            List<TPostItem> items = new ArrayList<>();

            for (TPostItem postItem : postItems) {
                synchronized (postItem) {
                    if (StringUtils.isNotBlank(postItem.getImagePath())) {
                        try {
                            URL url = new URL(StringUtils.replace(postItem.getImagePath(), " ", "%20"));
                            URLConnection connection = url.openConnection();
                            connection.connect();

                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            InputStream is = null;
                            try {
                                is = connection.getInputStream();

                                byte[] bytebuff = new byte[4096];
                                int n;
                                while ((n = is.read(bytebuff)) > 0) {
                                    baos.write(bytebuff, 0, n);
                                }
                            } catch (Exception e) {
                                logger.error(e);
                            } finally {
                                is.close();
                                baos.close();
                            }

                            String[] imageName = postItem.getImagePath().split("/");
                            postItem.setImageName(imageName[imageName.length - 1]);
                            postItem.setImageStream(new ByteArrayInputStream(baos.toByteArray()));
                            if (postItem.getImageStream() != null) {
                                logger.info("Downloading " + postItem.getImageName() + " complete from " + postItem.getImagePath());
                            }
                        } catch (IOException ex) {
                            logger.error(ex.getMessage());
                        }
                    }
                    items.add(postItem);
                }
            }

            postItemList.setPostItems(items);
            //postItemList.setPostItems(postItems);
            trobexService.processPostItemSyncronization(postItemList);

            SimpleDateFormat dateFormat = new SimpleDateFormat("H:ss");
            logger.info("Post items job executed every @ " + trobex.getInterval() + " second");
            logger.info(dateFormat.format(new Date()) + "-" + postItemList.toString());
        } else {
            logger.error("Unexpected result : " + postItemList.getStatus());
        }
    }
}
