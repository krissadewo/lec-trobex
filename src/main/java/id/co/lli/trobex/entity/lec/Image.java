/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.entity.lec;

/**
 *
 * @author Cosa
 */
public class Image extends DomainObject {

    private String metadata = "";
    private byte[] binaryData = null;
    private String fileName = "";
    private int fileSize = 0;
    private String mimeType = "";

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public byte[] getBinaryData() {
        return binaryData;
    }

    public void setBinaryData(byte[] binaryData) {
        this.binaryData = binaryData;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    @Override
    public String toString() {
        return "Image{" + "metadata=" + metadata + ", binaryData=" + binaryData + ", fileName=" + fileName + ", fileSize=" + fileSize + ", mimeType=" + mimeType + '}';
    }

}
