/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.entity.lec;

/**
 *
 * @author kris
 */
public class RoomToCategory {

    private Room room;
    private RoomCategory roomCategory;

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public RoomCategory getRoomCategory() {
        return roomCategory;
    }

    public void setRoomCategory(RoomCategory roomCategory) {
        this.roomCategory = roomCategory;
    }

    @Override
    public String toString() {
        return "RoomToCategory{" + "room=" + room + ", roomCategory=" + roomCategory + '}';
    }
}
