/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.entity.trobex;

import java.util.List;

/**
 *
 * @author kris
 */
public class TRoomList {

    private String status;
    private List<TRoom> rooms;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<TRoom> getRooms() {
        return rooms;
    }

    public void setRooms(List<TRoom> rooms) {
        this.rooms = rooms;
    }

    @Override
    public String toString() {
        return "TRoomList{" + "status=" + status + ", rooms=" + rooms + '}';
    }
}
