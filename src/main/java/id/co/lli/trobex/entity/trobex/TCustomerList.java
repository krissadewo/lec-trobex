/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.entity.trobex;

import java.util.List;

/**
 *
 * @author kris
 */
public class TCustomerList {

    private String status;
    private List<TCustomer> customers;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<TCustomer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<TCustomer> customers) {
        this.customers = customers;
    }

    @Override
    public String toString() {
        return "CustomerList{" + "status=" + status + ", customers=" + customers + '}';
    }
}
