/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.entity.lec;

/**
 *
 * @author kris
 */
public class DomainObject {

    private Integer id;
    private String distantKey;
    private Integer vstamp = 0;
    private String name;

    public DomainObject() {
        // default constructor
    }

    public String getDistantKey() {
        return distantKey;
    }

    public void setDistantKey(String distantKey) {
        this.distantKey = distantKey;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getVstamp() {
        return vstamp;
    }

    public void setVstamp(Integer vstamp) {
        this.vstamp = vstamp;
    }

    @Override
    public String toString() {
        return "DomainObject{" + "id=" + id + ", distantKey=" + distantKey + ", vstamp=" + vstamp + ", name=" + name + '}';
    }
}
