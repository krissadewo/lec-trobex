/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.entity.lec;

import java.util.Date;

/**
 *
 * @author kris
 */
public class Customer extends DomainObject {

    private String firstName;
    private String title;
    private String vipCode;
    private CustomerStatus status;
    private Date departureDate;
    private Date arrivalDate;
    private String language;
    private Room room;

    public static enum CustomerStatus {
        INACTIVE, OPEN_BILL, NO_CONSUME;

        public static Customer.CustomerStatus getCustomerStatus(String pName) {
            if (pName == null) {
                return null;
            }
            for (CustomerStatus status : CustomerStatus.values()) {
                if (status.name().toLowerCase().equals(pName.toLowerCase())) {
                    return status;
                }
            }
            return null;
        }
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public CustomerStatus getStatus() {
        return status;
    }

    public void setStatus(CustomerStatus status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVipCode() {
        return vipCode;
    }

    public void setVipCode(String vipCode) {
        this.vipCode = vipCode;
    }

    @Override
    public String toString() {
        return "Customer{" + "firstName=" + firstName + ", title=" + title + ", vipCode=" + vipCode + ", status=" + status + ", departureDate=" + departureDate + ", arrivalDate=" + arrivalDate + ", language=" + language + ", room=" + room + '}';
    }

}
