/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.entity.lec;

/**
 *
 * @author Cosa
 */
public class SystemParameter extends DomainObject {

    /**
     * defines the different available System-Parameter. Name of is set as
     * systemParameter type.
     */
    public static enum ParamType {

        CURRENCY,
        PMS_SYSTEM_IP,
        PMS_SYSTEM_PORT,
        PMS_DEPARTMENT_CODE,
        POST_MINIBAR_TO_PMS,
        POST_UA_TO_PMS,
        POST_RESTAURANT_TO_PMS,
        POST_ACTIVITIES_TO_PMS,
        SERVER_ADDRESS,
        ADMISSION_AFTER_ARRIVAL_DATE,
        MOVIE_PREVIEW_LENGTH,
        KEDIS_MESSAGE_QUEUE_TIMEOUT,
        TROBEX_API_KEY;

        public static SystemParameter.ParamType getParamType(String pName) {
            if (pName == null) {
                return null;
            }
            for (SystemParameter.ParamType type : SystemParameter.ParamType.values()) {
                if (type.name().toLowerCase().contains(pName.toLowerCase())) {
                    return type;
                }
            }
            return null;
        }
    }
    
    private String value = "";
    private ParamType type;

    public SystemParameter() {
        this.type = ParamType.TROBEX_API_KEY;
    }
    
    public SystemParameter(ParamType type) {
        this.type = type;
    }
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ParamType getType() {
        return type;
    }

    public void setType(ParamType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "SystemParameter{" + "value=" + value + ", type=" + type + '}';
    }
    
    
}
