/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.entity.trobex;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author kris
 */
public class TRoom {

    @SerializedName("room_number")
    private String roomNumber;
    @SerializedName("room_type")
    private String roomType;

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    @Override
    public String toString() {
        return "TRoom{" + "roomNumber=" + roomNumber + ", roomType=" + roomType + '}';
    }
}
