/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.entity.trobex;

import java.util.List;

/**
 *
 * @author kris
 */
public class TPostItemList {

    private String status;
    private List<TPostItem> postItems;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<TPostItem> getPostItems() {
        return postItems;
    }

    public void setPostItems(List<TPostItem> postItems) {
        this.postItems = postItems;
    }

    @Override
    public String toString() {
        return "TPostItemList{" + "status=" + status + ", postItems=" + postItems + '}';
    }
}
