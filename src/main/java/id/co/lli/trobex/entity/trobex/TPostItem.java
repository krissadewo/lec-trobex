/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.entity.trobex;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.InputStream;

/**
 *
 * @author kris
 */
public class TPostItem {

    @SerializedName("item_code")
    private String code;
    @SerializedName("item_name")
    private String name;
    @SerializedName("grp_code")
    private String groupCode;
    @SerializedName("cat_code")
    private String categoryCode;
    @SerializedName("unit_code")
    private String unitCode;
    @SerializedName("curr_code")
    private String currency;
    @SerializedName("item_price")
    private String price;
    @SerializedName("item_discount")
    private String discount;
    @SerializedName("item_notes")
    private String notes;
    @SerializedName("item_image")
    private String imagePath;
    @SerializedName("description")
    private String description;
    @SerializedName("pos_code")
    private String posCode;
    @Expose
    private String checksum;
    @Expose
    private String imageName;
    @Expose
    private InputStream imageStream;

    public String getPosCode() {
        return posCode;
    }

    public void setPosCode(String posCode) {
        this.posCode = posCode;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public InputStream getImageStream() {
        return imageStream;
    }

    public void setImageStream(InputStream imageStream) {
        this.imageStream = imageStream;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String itemCode) {
        this.code = itemCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String itemName) {
        this.name = itemName;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    @Override
    public String toString() {
        return "TPostItem{" + "code=" + code + ", name=" + name + ", groupCode=" + groupCode + ", categoryCode=" + categoryCode + ", unitCode=" + unitCode + ", currency=" + currency + ", price=" + price + ", discount=" + discount + ", notes=" + notes + ", imagePath=" + imagePath + ", description=" + description + ", imageName=" + imageName + ", imageStream=" + imageStream + '}';
    }
}
