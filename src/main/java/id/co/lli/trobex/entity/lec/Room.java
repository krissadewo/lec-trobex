/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.entity.lec;

/**
 *
 * @author Cosa
 */
public class Room extends DomainObject {

    public static enum RoomStatus {

        CCUPIED, OPEN_BILL, FREE, SHARED_GUESTS_ONLY;

        public static Room.RoomStatus getRoomStatus(String pName) {
            if (pName == null) {
                return null;
            }
            for (Room.RoomStatus status : Room.RoomStatus.values()) {
                if (status.name().toLowerCase().contains(pName.toLowerCase())) {
                    return status;
                }
            }
            return null;
        }
    }
    private String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    private RoomStatus status = RoomStatus.FREE;

    public RoomStatus getStatus() {
        return status;
    }

    public void setStatus(RoomStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Room{" + "status=" + status + '}';
    }
}
