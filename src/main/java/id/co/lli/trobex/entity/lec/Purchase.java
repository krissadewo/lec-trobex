/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.entity.lec;

import java.util.Date;

/**
 *
 * @author Cosa
 */
public class Purchase extends DomainObject {

    public static enum PurchaseStatus {

        /** guest choose a purchase item - purchase in SC */
        IN_SHOPPINGCART,
        /** guest confirmed shopping cart, purchase was sent to the workstation */
        SENDING,
        /** Guest is gone - purchase is meaningless */
        BALANCED,
        /** purchase was revoked */
        REVOKED,
        /** means it is approved. Can be used to give the guest feedback */
        APPROVED,
        /** booking is canceled. not relevant for billing */
        REJECTED;
    }
    
    private String description;
    private String localizedDescription;
    private Integer sequenceNumber = 0;
    private Integer checkNumber = 0;
    private double price;
    private int quantity = 1;
    private Room room;
    private PurchaseItem purchaseItem;
    private PurchaseStatus status;
    private Date created = new Date();
    private Date balanced = new Date();
    private Date valid = new Date();

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocalizedDescription() {
        return localizedDescription;
    }

    public void setLocalizedDescription(String localizedDescription) {
        this.localizedDescription = localizedDescription;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public Integer getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(Integer checkNumber) {
        this.checkNumber = checkNumber;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public PurchaseItem getPurchaseItem() {
        return purchaseItem;
    }

    public void setPurchaseItem(PurchaseItem purchaseItem) {
        this.purchaseItem = purchaseItem;
    }

    public PurchaseStatus getStatus() {
        return status;
    }

    public void setStatus(PurchaseStatus status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getBalanced() {
        return balanced;
    }

    public void setBalanced(Date balanced) {
        this.balanced = balanced;
    }

    public Date getValid() {
        return valid;
    }

    public void setValid(Date valid) {
        this.valid = valid;
    }

    @Override
    public String toString() {
        return "Purchase{" + "description=" + description + ", localizedDescription=" + localizedDescription + ", sequenceNumber=" + sequenceNumber + ", checkNumber=" + checkNumber + ", price=" + price + ", quantity=" + quantity + ", room=" + room + ", purchaseItem=" + purchaseItem + ", status=" + status + ", created=" + created + ", balanced=" + balanced + ", valid=" + valid + '}';
    }

}
