/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.entity.trobex;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 *
 * @author kris
 */
public class TCustomer implements Serializable {

    @SerializedName("reservation_code")
    private String reservationCode;
    @SerializedName("check_in_date")
    private String checkInDate;
    @SerializedName("check_out_date")
    private String checkOutDate;
    @SerializedName("guest_code")
    private String guestCode;
    @SerializedName("guest_title")
    private String guestTitle;
    @SerializedName("guest_frst_name")
    private String guestFristName;
    @SerializedName("guest_last_name")
    private String guestLastName;
    @SerializedName("guest_nationality")
    private String guestNationality;
    @SerializedName("guest_alias")
    private String guestAlias;
    @SerializedName("guest_type")
    private String guestType;
    @SerializedName("address")
    private String address;
    @SerializedName("city")
    private String city;
    @SerializedName("state")
    private String state;
    @SerializedName("phone_number")
    private String phoneNumber;
    @SerializedName("handphone_number")
    private String handphoneNumber;
    @SerializedName("guest_sex_type")
    private String sex;
    @SerializedName("room_type")
    private String roomType;
    @SerializedName("room_number")
    private String roomNumber;
    @SerializedName("total_person")
    private String totalPerson;
    @SerializedName("guest_status")
    private String guestStatus;
    @SerializedName("status")
    private String status;

    public String getReservationCode() {
        return reservationCode;
    }

    public void setReservationCode(String reservationCode) {
        this.reservationCode = reservationCode;
    }

    public String getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    public String getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public String getGuestCode() {
        return guestCode;
    }

    public void setGuestCode(String guestCode) {
        this.guestCode = guestCode;
    }

    public String getGuestTitle() {
        return guestTitle;
    }

    public void setGuestTitle(String guestTitle) {
        this.guestTitle = guestTitle;
    }

    public String getGuestFristName() {
        return guestFristName;
    }

    public void setGuestFristName(String guestFristName) {
        this.guestFristName = guestFristName;
    }

    public String getGuestLastName() {
        return guestLastName;
    }

    public void setGuestLastName(String guestLastName) {
        this.guestLastName = guestLastName;
    }

    public String getGuestNationality() {
        return guestNationality;
    }

    public void setGuestNationality(String guestNationality) {
        this.guestNationality = guestNationality;
    }

    public String getGuestAlias() {
        return guestAlias;
    }

    public void setGuestAlias(String guestAlias) {
        this.guestAlias = guestAlias;
    }

    public String getGuestType() {
        return guestType;
    }

    public void setGuestType(String guestType) {
        this.guestType = guestType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getHandphoneNumber() {
        return handphoneNumber;
    }

    public void setHandphoneNumber(String handphoneNumber) {
        this.handphoneNumber = handphoneNumber;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getTotalPerson() {
        return totalPerson;
    }

    public void setTotalPerson(String totalPerson) {
        this.totalPerson = totalPerson;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGuestStatus() {
        return guestStatus;
    }

    public void setGuestStatus(String guestStatus) {
        this.guestStatus = guestStatus;
    }

    @Override
    public String toString() {
        return "Guest{" + "reservationCode=" + reservationCode + ", checkInDate=" + checkInDate + ", checkOutDate=" + checkOutDate + ", guestCode=" + guestCode + ", guestTitle=" + guestTitle + ", guestFristName=" + guestFristName + ", guestLastName=" + guestLastName + ", guestNationality=" + guestNationality + ", guestAlias=" + guestAlias + ", guestType=" + guestType + ", address=" + address + ", city=" + city + ", state=" + state + ", phoneNumber=" + phoneNumber + ", handphoneNumber=" + handphoneNumber + ", sex=" + sex + ", roomType=" + roomType + ", roomNumber=" + roomNumber + ", totalPerson=" + totalPerson + ", status=" + status + '}';
    }
}
