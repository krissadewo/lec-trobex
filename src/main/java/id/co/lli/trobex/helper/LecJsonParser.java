/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.helper;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author kris
 */
@Component
public class LecJsonParser {

    private static Logger logger = Logger.getLogger(LecJsonParser.class);
    @Autowired
    private Gson gson;
    @Autowired
    private JsonParser jsonParser;

    public JsonObject getJsonObject(String url, String key) {
        try {
            String urlName = url + "?apikey=" + key;
            logger.info(urlName);
            return jsonParser.parse(readUrl(urlName)).getAsJsonObject();
        } catch (Exception ex) {
            logger.log(Level.ERROR, ex);
        }
        return null;
    }

    private String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuilder builder = new StringBuilder();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1) {
                builder.append(chars, 0, read);
            }
            return builder.toString();
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }

    public Gson getGson() {
        return gson;
    }
}
