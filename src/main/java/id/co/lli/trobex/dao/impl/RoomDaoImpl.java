/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao.impl;

import id.co.lli.trobex.dao.RoomDao;
import id.co.lli.trobex.entity.lec.Room;
import id.co.lli.trobex.entity.lec.Room.RoomStatus;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Cosa
 */
@Repository
public class RoomDaoImpl implements RoomDao {

    private static Logger logger = Logger.getLogger(RoomDaoImpl.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void save(final Room entity) {
        String sql = "INSERT INTO room(distant_key, vstamp, name, status) VALUES(?,?,?,?)";
        jdbcTemplate.update(sql, new Object[]{
                    entity.getDistantKey(),
                    entity.getVstamp(),
                    entity.getName(),
                    entity.getStatus().ordinal()
                });
    }

    @Override
    public void save(final List<Room> rooms) {
        String sql = "INSERT INTO room(distant_key, vstamp, name, status) VALUES(?,?,?,?)";
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Room room = rooms.get(i);
                ps.setString(1, room.getDistantKey());
                ps.setInt(2, room.getVstamp());
                ps.setString(3, room.getName());
                ps.setInt(4, room.getStatus().ordinal());
            }

            @Override
            public int getBatchSize() {
                return rooms.size();
            }
        });
    }

    @Override
    public void update(final List<Room> rooms) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void delete(Room entity) {
    }

    @Override
    public void update(Room entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Room> getAll() {
        String sql = "SELECT * FROM room";
        try {
            return jdbcTemplate.query(sql, new RoomMapper());
        } catch (DataAccessException exception) {
            logger.error(exception.getMessage());
        }
        return null;
    }

    @Override
    public Room getById(Integer roomId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Room getByDistantKey(String distantKey) {
        String sql = "SELECT * FROM room WHERE distant_key = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{distantKey}, new RoomMapper());
        } catch (DataAccessException exception) {
            logger.error(exception.getMessage());
        }
        return null;

    }

    class RoomMapper implements RowMapper<Room> {

        @Override
        public Room mapRow(ResultSet rs, int rowNum) throws SQLException {
            ResultSetMetaData rsmd = rs.getMetaData();
            Room room = new Room();

            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                switch (rsmd.getColumnLabel(i)) {
                    case "id":
                        room.setId(rs.getInt("id"));
                        break;
                    case "name":
                        room.setName(rs.getString("name"));
                        break;
                    case "distant_key":
                        room.setDistantKey(rs.getString("distant_key"));
                        break;
                    case "status":
                        room.setStatus(RoomStatus.getRoomStatus(rs.getString("status")));
                        break;
                    default:
                }
            }

            return room;

        }
    }
}
