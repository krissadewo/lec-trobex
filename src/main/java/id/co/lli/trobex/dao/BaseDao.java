/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao;

import java.util.List;

/**
 *
 * @author kris
 */
public interface BaseDao<T> {

    void save(T entity);

    void delete(T entity);

    void update(T entity);

    List<T> getAll();

    T getById(Integer id);
}
