/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao.impl;

import id.co.lli.trobex.dao.SystemParameterDao;
import id.co.lli.trobex.entity.lec.Room;
import id.co.lli.trobex.entity.lec.SystemParameter;
import id.co.lli.trobex.entity.lec.SystemParameter.ParamType;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class SystemParameterDaoImpl implements SystemParameterDao {

    private static Logger logger = Logger.getLogger(SystemParameterDaoImpl.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void save(final SystemParameter entity) {
        String sql = "INSERT INTO systemparameter(distant_key, vstamp, name, parameter_type, value) VALUES(?,?,?,?,?)";
        jdbcTemplate.update(sql, new Object[]{
                    entity.getDistantKey(),
                    entity.getVstamp(),
                    entity.getType().name(),
                    entity.getType().ordinal(),
                    entity.getValue()
                });
    }

    @Override
    public SystemParameter getById(Integer systemParameterId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void delete(SystemParameter entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void update(SystemParameter entity) {
        String sql = "UPDATE systemparameter SET value=? WHERE parameter_type=?";
        jdbcTemplate.update(sql, new Object[]{
                    entity.getValue(),
                    entity.getType().ordinal()
                });
    }

    @Override
    public List<SystemParameter> getAll() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public SystemParameter getByType(ParamType paramType) {
        String sql = "SELECT * FROM systemparameter WHERE parameter_type = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{paramType.ordinal()}, new SystemParameterMapper());
        } catch (DataAccessException exception) {
            logger.error(exception.getMessage());
        }
        return null;
    }

    class SystemParameterMapper implements RowMapper<SystemParameter> {

        @Override
        public SystemParameter mapRow(ResultSet rs, int rowNum) throws SQLException {
            ResultSetMetaData rsmd = rs.getMetaData();
            SystemParameter systemParameter = new SystemParameter();

            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                switch (rsmd.getColumnLabel(i)) {
                    case "id":
                        systemParameter.setId(rs.getInt("id"));
                        break;
                    case "name":
                        systemParameter.setName(rs.getString("name"));
                        break;
                    case "distant_key":
                        systemParameter.setDistantKey(rs.getString("distant_key"));
                        break;
                    case "value":
                        systemParameter.setValue(rs.getString("value"));
                        break;
                    default:
                }
            }

            return systemParameter;

        }
    }
}
