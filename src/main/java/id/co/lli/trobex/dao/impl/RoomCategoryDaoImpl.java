/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao.impl;

import id.co.lli.trobex.dao.RoomCategoryDao;
import id.co.lli.trobex.entity.lec.RoomCategory;
import id.co.lli.trobex.entity.lec.RoomToCategory;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kris
 */
@Repository
public class RoomCategoryDaoImpl implements RoomCategoryDao {

    private static Logger logger = Logger.getLogger(RoomCategoryDaoImpl.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void save(RoomCategory entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void save(final List<RoomCategory> roomCategorys) {
        String sql = "INSERT INTO room_category(distant_key, vstamp, name) VALUES(?,?,?)";
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                RoomCategory roomCategory = roomCategorys.get(i);
                ps.setString(1, roomCategory.getDistantKey());
                ps.setInt(2, roomCategory.getVstamp());
                ps.setString(3, roomCategory.getName());
            }

            @Override
            public int getBatchSize() {
                return roomCategorys.size();
            }
        });
    }

    @Override
    public void saveRoomToCategory(final List<RoomToCategory> roomToCategorys) {
        String sql = "INSERT INTO room_2_category(room_id, categorie_id) VALUES(?,?)";
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                RoomToCategory roomToCategory = roomToCategorys.get(i);
                ps.setInt(1, roomToCategory.getRoom().getId());
                ps.setInt(2, roomToCategory.getRoomCategory().getId());
            }

            @Override
            public int getBatchSize() {
                return roomToCategorys.size();
            }
        });
    }

    @Override
    public void delete(RoomCategory entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void update(RoomCategory entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<RoomCategory> getAll() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public RoomCategory getById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public RoomCategory getByDistantKey(String distantKey) {
        String sql = "SELECT *FROM room_category WHERE distant_key = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{distantKey}, new RoomCategoryMapper());
        } catch (DataAccessException exception) {
            logger.error(exception.getMessage());
        }
        return null;
    }

    @Override
    public RoomCategory getByName(String name) {
        String sql = "SELECT id, name, distant_key FROM room_category WHERE name = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{name}, new RoomCategoryMapper());
        } catch (DataAccessException exception) {
            logger.error(exception.getMessage());
        }
        return null;
    }

    class RoomCategoryMapper implements RowMapper<RoomCategory> {

        @Override
        public RoomCategory mapRow(ResultSet rs, int rowNum) throws SQLException {
            ResultSetMetaData rsmd = rs.getMetaData();
            RoomCategory roomCategory = new RoomCategory();

            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                switch (rsmd.getColumnLabel(i)) {
                    case "id":
                        roomCategory.setId(rs.getInt("id"));
                        break;
                    case "name":
                        roomCategory.setName(rs.getString("name"));
                        break;
                    case "distant_key":
                        roomCategory.setDistantKey(rs.getString("distant_key"));
                        break;
                    default:

                }
            }
            return roomCategory;
        }
    }
}
