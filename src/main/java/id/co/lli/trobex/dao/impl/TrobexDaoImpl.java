/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao.impl;

import id.co.lli.trobex.dao.TrobexDao;
import id.co.lli.trobex.entity.trobex.Trobex;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 *
 * @author kris
 */
@Component
public class TrobexDaoImpl implements TrobexDao {

    private static Logger logger = Logger.getLogger(TrobexDaoImpl.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void save(Trobex entity) {
        String sql = "INSERT INTO trobex(service_url, time_interval, job_key) VALUES(?,?,?)";
        jdbcTemplate.update(sql, new Object[]{                    
                    entity.getServiceUrl(),
                    entity.getInterval(),
                    entity.getJobKey(),                   
                });
    }

    @Override
    public void delete(Trobex entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void update(Trobex entity) {
        String sql = "UPDATE trobex SET service_url=?, time_interval=? WHERE job_key=?";
        jdbcTemplate.update(sql, new Object[]{
                    entity.getServiceUrl(),
                    entity.getInterval(),                  
                    entity.getJobKey()
                });
    }

    @Override
    public List<Trobex> getAll() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Trobex getById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Trobex getByJobKey(String jobKey) {
        String sql = "SELECT * FROM trobex WHERE job_key = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{jobKey}, new TrobexMapper());
        } catch (DataAccessException exception) {
            logger.error(exception.getMessage());
        }
        return null;
    }

    class TrobexMapper implements RowMapper<Trobex> {

        @Override
        public Trobex mapRow(ResultSet rs, int rowNum) throws SQLException {
            Trobex trobex = new Trobex();
            trobex.setId(rs.getInt("id"));
            trobex.setJobKey(rs.getString("job_key"));
            trobex.setInterval(rs.getInt("time_interval"));
            trobex.setServiceUrl(rs.getString("service_url"));         
            return trobex;
        }
    }
}
