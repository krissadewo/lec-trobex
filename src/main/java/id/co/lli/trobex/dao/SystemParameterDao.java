/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao;

import id.co.lli.trobex.entity.lec.SystemParameter;
import id.co.lli.trobex.entity.lec.SystemParameter.ParamType;

/**
 *
 * @author Cosa
 */
public interface SystemParameterDao extends BaseDao<SystemParameter> {
    
    SystemParameter getByType(ParamType paramType);
}
