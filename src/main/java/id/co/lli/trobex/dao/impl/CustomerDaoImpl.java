/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao.impl;

import id.co.lli.trobex.dao.CustomerDao;
import id.co.lli.trobex.entity.lec.Customer;
import id.co.lli.trobex.entity.lec.Customer.CustomerStatus;
import id.co.lli.trobex.entity.lec.Room;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kris
 */
@Repository
public class CustomerDaoImpl implements CustomerDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    @Transactional
    public void save(final Customer entity) {
        String sql = "INSERT INTO customer(vstamp, name, first_name, arrival_date, shared_room_id) VALUES(?,?,?,?,?)";
        jdbcTemplate.update(sql, new Object[]{
                    entity.getVstamp(),
                    entity.getName(),
                    entity.getFirstName(),
                    entity.getArrivalDate(),
                    entity.getRoom()
                });
    }

    @Override
    public void delete(Customer entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void update(Customer entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Customer> getAll() {
        String sql = "SELECT *FROM customer c"
                + " INNER JOIN room r"
                + " ON r.id = c.shared_room_id";
        return jdbcTemplate.query(sql, new CustomerMapper());
    }

    @Override
    public Customer getById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    class CustomerMapper implements RowMapper<Customer> {

        @Override
        public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
            ResultSetMetaData rsmd = rs.getMetaData();
            Customer customer = new Customer();

            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                switch (rsmd.getColumnLabel(i)) {
                    case "id":
                        customer.setId(rs.getInt("id"));
                        break;
                    case "name":
                        customer.setName(rs.getString("name"));
                        break;
                    case "first_name":
                        customer.setFirstName(rs.getString("first_name"));
                        break;
                    case "title":
                        customer.setTitle(rs.getString("title"));
                        break;
                    case "distant_key":
                        customer.setDistantKey(rs.getString("distant_key"));
                        break;
                    case "arrival_date":
                        customer.setArrivalDate(rs.getDate("arrival_date"));
                        break;
                    case "departure_date":
                        customer.setDepartureDate(rs.getDate("departure_date"));
                        break;
                    case "language":
                        customer.setLanguage(rs.getString("language"));
                        break;
                    case "vip_code":
                        customer.setLanguage(rs.getString("vip_code"));
                        break;
                    case "shared_room_id":
                        Room room = new Room();
                        room.setId(rs.getInt("shared_room_id"));
                        customer.setRoom(room);
                        break;
                    case "status":
                        customer.setStatus(CustomerStatus.getCustomerStatus(rs.getString("status")));
                        break;
                    default:
                }
            }

            return customer;
        }
    }
}
