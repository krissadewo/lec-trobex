
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao.service;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import id.co.lli.trobex.App;
import id.co.lli.trobex.dao.PurchaseItemDao;
import id.co.lli.trobex.dao.RoomCategoryDao;
import id.co.lli.trobex.dao.RoomDao;
import id.co.lli.trobex.dao.SystemParameterDao;
import id.co.lli.trobex.dao.TrobexDao;
import id.co.lli.trobex.entity.lec.Room;
import id.co.lli.trobex.entity.lec.RoomCategory;
import id.co.lli.trobex.entity.lec.RoomToCategory;
import id.co.lli.trobex.entity.lec.SystemParameter;
import id.co.lli.trobex.entity.trobex.TPostItemList;
import id.co.lli.trobex.entity.trobex.TRoom;
import id.co.lli.trobex.entity.trobex.TRoomList;
import id.co.lli.trobex.entity.trobex.Trobex;
import id.co.lli.trobex.helper.LecJsonParser;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author kris
 */
@Service
public class TrobexService {

    private static Logger logger = Logger.getLogger(TrobexService.class);
    @Autowired
    private TrobexDao trobexDao;
    @Autowired
    private RoomDao roomDao;
    @Autowired
    private RoomCategoryDao roomCategoryDao;
    @Autowired
    private PurchaseItemDao purchaseItemDao;
    @Autowired
    public LecJsonParser lecJsonParser;
    @Autowired
    public SystemParameterDao systemParameterDao;

    public List<Trobex> getAll() {
        return trobexDao.getAll();
    }

    public Trobex getByJobKey(String jobKey) {
        return trobexDao.getByJobKey(jobKey);
    }

    public void update(Trobex trobex) {
        trobexDao.update(trobex);
    }

    public void save(Trobex trobex) {
        trobexDao.save(trobex);
    }

    public void processPostItemSyncronization(TPostItemList postItemList) {
        purchaseItemDao.createPostItemData(postItemList.getPostItems());
    }

    public String processRoomSynchronization() {
        //Map<String, Object> model = new HashMap<>();
        //process data to service
        Trobex trobex = this.getByJobKey(App.JOB_KEY_ROOM);
        JsonObject jsonObject = lecJsonParser.getJsonObject(trobex.getServiceUrl(), systemParameterDao.getByType(SystemParameter.ParamType.TROBEX_API_KEY).getValue());
        TRoomList roomList = lecJsonParser.getGson().fromJson(jsonObject, TRoomList.class);
        if (roomList != null) {
            if (roomList.getStatus().equals(App.EXPECTED_RESULT)) {
                List<TRoom> rooms = lecJsonParser.getGson().fromJson(jsonObject.getAsJsonArray(App.KEY_DATA), new TypeToken<List<TRoom>>() {
                }.getType());
                roomList.setRooms(rooms);
                //List<Room> updateListRoom = new ArrayList<>();
                //Map<String, String> updateListCategory = new HashMap<>();

                List<Room> saveListRooms = new ArrayList<>();
                Map<String, String> saveListCategory = new HashMap<>();

                for (TRoom tRoom : roomList.getRooms()) {
                    Room room = roomDao.getByDistantKey(tRoom.getRoomNumber());
                    if (room == null) {
                        room = new Room();
                        room.setDistantKey(tRoom.getRoomNumber());
                        room.setStatus(Room.RoomStatus.FREE);
                        room.setName(tRoom.getRoomNumber());
                        room.setCategory(tRoom.getRoomType());
                        saveListRooms.add(room);
                    } else {
                        //updateListRoom.add(room);
                        logger.info("No room update can't be processed here");
                    }

                    RoomCategory roomCategory = roomCategoryDao.getByName(tRoom.getRoomType());
                    if (roomCategory == null) {
                        saveListCategory.put(tRoom.getRoomType(), tRoom.getRoomType());
                    } else {
                        logger.info("No room category update can't be processed here");
                    }
                }

                //Save room
                if (saveListRooms.size() > 0) {
                    roomDao.save(saveListRooms);
                }
                //Save room category
                if (saveListCategory.size() > 0) {
                    List<RoomCategory> roomCategorys = new ArrayList<>();
                    for (String roomType : saveListCategory.values()) {
                        RoomCategory roomCategory = new RoomCategory();
                        roomCategory.setName(roomType);
                        roomCategory.setDistantKey(roomType);
                        roomCategorys.add(roomCategory);
                    }
                    roomCategoryDao.save(roomCategorys);
                }

                //Save room to category
                if (saveListRooms.size() > 0) {
                    List<RoomToCategory> rtcs = new ArrayList<>();
                    for (Room room : saveListRooms) {
                        RoomToCategory rtc = new RoomToCategory();
                        rtc.setRoom(roomDao.getByDistantKey(room.getDistantKey()));
                        rtc.setRoomCategory(roomCategoryDao.getByDistantKey(room.getCategory()));
                        rtcs.add(rtc);
                    }
                    roomCategoryDao.saveRoomToCategory(rtcs);
                }
                return "Success ";
            } else {
                return "Unexpected result " + roomList.getStatus();
            }
        }
        return null;
    }

    public SystemParameter getTrobexApiKey() {
        return systemParameterDao.getByType(SystemParameter.ParamType.TROBEX_API_KEY);
    }

    public void updateTrobexApiKey(SystemParameter systemParameter) {
        systemParameterDao.update(systemParameter);
    }

    public void saveTrobexApiKey(SystemParameter systemParameter) {
        systemParameterDao.save(systemParameter);
    }
}
