/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao;

import id.co.lli.trobex.entity.trobex.Trobex;

/**
 *
 * @author kris
 */
public interface TrobexDao extends BaseDao<Trobex> {

    Trobex getByJobKey(String jobKey);
}
