/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao;

import id.co.lli.trobex.entity.lec.RoomCategory;
import id.co.lli.trobex.entity.lec.RoomToCategory;
import java.util.List;

/**
 *
 * @author kris
 */
public interface RoomCategoryDao extends BaseDao<RoomCategory> {

    RoomCategory getByDistantKey(String distantKey);

    RoomCategory getByName(String name);

    void save(final List<RoomCategory> roomCategorys);

    void saveRoomToCategory(final List<RoomToCategory> roomToCategorys);
}
