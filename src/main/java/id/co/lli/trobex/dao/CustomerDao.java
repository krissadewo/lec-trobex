/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao;

import id.co.lli.trobex.entity.lec.Customer;

/**
 *
 * @author kris
 */
public interface CustomerDao extends BaseDao<Customer> {
}
