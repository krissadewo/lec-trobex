/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao.service;

import id.co.lli.trobex.dao.CustomerDao;
import id.co.lli.trobex.entity.lec.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author kris
 */
@Service
public class CustomerService {

    @Autowired
    private CustomerDao customerDao;

    public void save(Customer customer) {
        customerDao.save(customer);
    }
}
