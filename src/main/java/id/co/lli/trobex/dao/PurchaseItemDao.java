/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao;

import id.co.lli.trobex.entity.lec.PurchaseItem;
import id.co.lli.trobex.entity.trobex.TPostItem;
import java.util.List;

/**
 *
 * @author kris
 */
public interface PurchaseItemDao extends BaseDao<PurchaseItem> {

    void createPostItemData(List<TPostItem> postItems);
}
