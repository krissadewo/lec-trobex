/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao.impl;

import id.co.lli.trobex.dao.PurchaseItemDao;
import id.co.lli.trobex.entity.lec.PurchaseItem;
import id.co.lli.trobex.entity.trobex.TPostItem;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kris
 */
@Repository
public class PurchaseItemDaoImpl implements PurchaseItemDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void save(PurchaseItem entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void delete(PurchaseItem entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void update(PurchaseItem entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<PurchaseItem> getAll() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public PurchaseItem getById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void createPostItemData(final List<TPostItem> postItems) {
        String sqlDropTable = "DROP TABLE IF EXISTS temp_post_item";
        String sqlCreateTable = "CREATE TABLE IF NOT EXISTS temp_post_item ("
                + "code VARCHAR(255) NULL DEFAULT NULL COLLATE 'latin1_general_cs',"
                + "name VARCHAR(255),"
                + "grp_code VARCHAR(255),"
                + "cat_code VARCHAR(255),"
                + "unit_code VARCHAR(255),"
                + "curr_code VARCHAR(255),"
                + "price VARCHAR(255),"
                + "discount VARCHAR(255),"
                + "notes VARCHAR(255),"
                + "image_path VARCHAR(255),"
                + "image_binary LONGBLOB,"
                + "image_name VARCHAR(255),"
                + "description VARCHAR(255),"
                + "checksum VARCHAR(255),"
                + "pos_code VARCHAR(255) NULL DEFAULT NULL COLLATE 'latin1_general_cs')";
        String sqlInsert = "INSERT INTO temp_post_item(code, name, grp_code, cat_code, unit_code, curr_code, price, discount, notes, image_path, image_binary, image_name, description, checksum, pos_code) "
                + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,CONVERT(MD5(CONCAT(?)) USING latin1),?)";
        jdbcTemplate.execute(sqlDropTable);
        jdbcTemplate.execute(sqlCreateTable);
        jdbcTemplate.batchUpdate(sqlInsert, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                TPostItem item = postItems.get(i);
                ps.setString(1, item.getCode());
                ps.setString(2, item.getName());
                ps.setString(3, item.getGroupCode());
                ps.setString(4, item.getCategoryCode());
                ps.setString(5, item.getUnitCode());
                ps.setString(6, item.getCurrency());
                ps.setString(7, item.getPrice());
                ps.setString(8, item.getDiscount());
                ps.setString(9, item.getNotes());
                ps.setString(10, item.getImagePath());
                ps.setBlob(11, item.getImageStream());
                ps.setString(12, item.getImageName());
                ps.setString(13, item.getDescription());
                ps.setString(14, item.getCode() + item.getName() + item.getPrice() + item.getDescription());
                ps.setString(15, item.getPosCode());
            }

            @Override
            public int getBatchSize() {
                return postItems.size();
            }
        });

       jdbcTemplate.update("CALL sp_sync_item;");
    }
}