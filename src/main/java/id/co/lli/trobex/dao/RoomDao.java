/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.dao;

import id.co.lli.trobex.entity.lec.Room;
import java.util.List;

/**
 *
 * @author Cosa
 */
public interface RoomDao extends BaseDao<Room> {

    Room getByDistantKey(String distantKey);

    void save(final List<Room> rooms);

    void update(final List<Room> rooms);
}
