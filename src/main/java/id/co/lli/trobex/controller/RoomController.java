/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.controller;

import id.co.lli.trobex.App;
import id.co.lli.trobex.entity.trobex.Trobex;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Cosa
 */
@Controller
@RequestMapping(value = {"/room"})
public class RoomController extends BaseController {

    @RequestMapping(method = RequestMethod.GET)
    public ModelMap showForm(ModelMap model) {
        Trobex trobex = trobexService.getByJobKey(App.JOB_KEY_ROOM);
        if (trobex == null) {
            trobex = new Trobex();
        }
        model.addAttribute("trobex", trobex);
        return model;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView submitForm(@ModelAttribute Trobex pTrobex) {
        Trobex trobex = trobexService.getByJobKey(App.JOB_KEY_ROOM);
        if (trobex != null) {
            trobex.setServiceUrl(pTrobex.getServiceUrl());
            trobex.setInterval(pTrobex.getInterval());
            trobexService.update(trobex);
        } else {
            pTrobex.setJobKey(App.JOB_KEY_ROOM);
            trobexService.save(pTrobex);
        }

        Map<String, Object> model = new HashMap<>();
        if (trobexService.getByJobKey(App.JOB_KEY_ROOM) != null) {
            model.put(App.KEY_STATUS, trobexService.processRoomSynchronization());
        }
        return new ModelAndView("room", model);
    }
}
