/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.controller;

import id.co.lli.trobex.dao.service.TrobexService;
import id.co.lli.trobex.helper.LecJsonParser;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author kris
 */
public class BaseController {

    @Autowired
    public TrobexService trobexService;
    @Autowired
    public LecJsonParser lecJsonParser;
}
