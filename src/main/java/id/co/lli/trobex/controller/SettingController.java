/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.controller;

import id.co.lli.trobex.App;
import id.co.lli.trobex.entity.lec.SystemParameter;
import id.co.lli.trobex.entity.trobex.Trobex;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author kris
 */
@Controller
@RequestMapping("/settingApiKey")
public class SettingController extends BaseController {

    @RequestMapping(method = RequestMethod.GET)
    public ModelMap showForm(ModelMap model) {
        SystemParameter systemParameter = trobexService.getTrobexApiKey();
        if (systemParameter == null) {
            systemParameter = new SystemParameter();
        }
        model.addAttribute("systemParameter", systemParameter);
        return model;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String submitForm(@ModelAttribute SystemParameter pSystemParameter) {
        SystemParameter systemParameter = trobexService.getTrobexApiKey();
        if (systemParameter != null) {
            systemParameter.setValue(pSystemParameter.getValue());
            trobexService.updateTrobexApiKey(systemParameter);
        } else {
            trobexService.saveTrobexApiKey(pSystemParameter);
        }

        return "settingApiKey";
    }
}
