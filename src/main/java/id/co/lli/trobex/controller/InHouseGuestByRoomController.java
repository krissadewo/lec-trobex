/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.controller;

import id.co.lli.trobex.App;
import id.co.lli.trobex.entity.trobex.Trobex;
import id.co.lli.trobex.scheduler.service.InHouseScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Cosa
 */
@Controller
@RequestMapping(value = {"/inhouseguestbyroom"})
public class InHouseGuestByRoomController extends BaseController {

    @Autowired
    private InHouseScheduler inHouseScheduler;

    @RequestMapping(method = RequestMethod.GET)
    public ModelMap showForm(ModelMap model) {
        Trobex trobex = trobexService.getByJobKey(App.JOB_KEY_INHOUSE_BY_ROOM);
        if (trobex == null) {
            trobex = new Trobex();
        }
        model.addAttribute("trobex", trobex);
        return model;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String submitForm(@ModelAttribute Trobex pTrobex) {
        Trobex trobex = trobexService.getByJobKey(App.JOB_KEY_INHOUSE_BY_ROOM);
        if (trobex != null) {
            trobex.setServiceUrl(pTrobex.getServiceUrl());
            trobex.setInterval(0);           
            trobexService.update(trobex);
        } else {
            pTrobex.setJobKey(App.JOB_KEY_INHOUSE_BY_ROOM);
            trobex.setInterval(0);           
            trobexService.save(pTrobex);
        }

        return "inhouseguestbyroom";
    }
}
