/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.controller;

import id.co.lli.trobex.App;
import id.co.lli.trobex.dao.service.TrobexService;
import id.co.lli.trobex.entity.trobex.TRoom;
import id.co.lli.trobex.entity.trobex.TRoomList;
import id.co.lli.trobex.entity.trobex.Trobex;
import id.co.lli.trobex.helper.LecJsonParser;
import id.co.lli.trobex.scheduler.service.InHouseScheduler;
import id.co.lli.trobex.scheduler.service.PostItemScheduler;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author kris
 */
@Controller
@RequestMapping(value = "/test")
public class TestController {

    @Autowired
    private InHouseScheduler inHouseAllScheduler;
    @Autowired
    private PostItemScheduler postItemsScheduler;
    @Autowired
    private LecJsonParser lecJsonParser;
    @Autowired
    private TrobexService trobexService;

    @RequestMapping(value = "/ih/start")
    public String startInHouseJob() {
        inHouseAllScheduler.process();
        return "index";
    }

    @RequestMapping(value = "/ih/reset")
    public String resetInHouseJob() {
        inHouseAllScheduler.reset();
        return "index";
    }

    @RequestMapping(value = "/pi/start")
    public String startPiJob() {
        postItemsScheduler.process();
        return "index";
    }

    @RequestMapping(value = "/pi/reset")
    public String resetPiJob() {
        postItemsScheduler.reset();
        return "index";
    }
}
