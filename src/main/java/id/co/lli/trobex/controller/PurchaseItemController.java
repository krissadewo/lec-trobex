/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.controller;

import id.co.lli.trobex.App;
import id.co.lli.trobex.entity.trobex.Trobex;
import id.co.lli.trobex.scheduler.service.PostItemScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author kris
 */
@Controller
@RequestMapping("/purchaseItem")
public class PurchaseItemController extends BaseController {

    @Autowired
    private PostItemScheduler postItemScheduler;

    @RequestMapping(method = RequestMethod.GET)
    public ModelMap showForm(ModelMap model) {
        Trobex trobex = trobexService.getByJobKey(App.JOB_KEY_PURCHASE_ITEM);
        if (trobex == null) {
            trobex = new Trobex();
        }
        model.addAttribute("trobex", trobex);
        return model;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String submitForm(@ModelAttribute Trobex pTrobex) {
        Trobex trobex = trobexService.getByJobKey(App.JOB_KEY_PURCHASE_ITEM);
        if (trobex != null) {
            trobex.setServiceUrl(pTrobex.getServiceUrl());
            trobex.setInterval(pTrobex.getInterval());           
            trobexService.update(trobex);
        } else {
            pTrobex.setJobKey(App.JOB_KEY_PURCHASE_ITEM);
            trobexService.save(pTrobex);
        }

        if (trobexService.getByJobKey(App.JOB_KEY_PURCHASE_ITEM) != null) {
            postItemScheduler.process();
        }

        return "purchaseItem";
    }
}
