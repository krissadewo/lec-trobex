/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.lli.trobex.controller;

import id.co.lli.trobex.App;
import id.co.lli.trobex.entity.trobex.Trobex;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author kris
 */
@Controller
@RequestMapping("/postRestaurant")
public class PostRestaurantController extends BaseController {

    @RequestMapping(method = RequestMethod.GET)
    public ModelMap showForm(ModelMap model) {
        Trobex trobex = trobexService.getByJobKey(App.JOB_KEY_POST_RESTAURANT);
        if (trobex == null) {
            trobex = new Trobex();
        }
        model.addAttribute("trobex", trobex);
        return model;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String submitForm(@ModelAttribute Trobex pTrobex) {
        Trobex trobex = trobexService.getByJobKey(App.JOB_KEY_POST_RESTAURANT);
        if (trobex != null) {
            trobex.setServiceUrl(pTrobex.getServiceUrl());
            trobex.setInterval(0);           
            trobexService.update(trobex);
        } else {
            pTrobex.setJobKey(App.JOB_KEY_POST_RESTAURANT);
            pTrobex.setInterval(0);           
            trobexService.save(pTrobex);
        }

        return "postRestaurant";
    }
}
