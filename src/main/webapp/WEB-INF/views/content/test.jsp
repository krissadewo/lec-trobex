<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
Status : ${status}
<br>
<c:if test="${not empty roomList.rooms}">
    <table> 
        <c:forEach var="room" items="${roomList.rooms}">
            <tr>
                <td>${room.roomNumber}</td>
                <td>${room.roomType}</td>
            </tr>
        </c:forEach>
    </table>

</c:if>
