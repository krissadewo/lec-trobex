<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${not empty status}">
    STATUS : ${status}
</c:if>
<body>
    <form:form modelAttribute="trobex">
        <table border="0" cellspacing="3" width="800">
            <tr>
                <td width="100">URL</td>
                <td><form:input path="serviceUrl" maxlength="200" size="100"/></td>
            </tr>
            <tr>
                <td>Interval</td>
                <td><form:input path="interval" maxlength="5" size="7"/>&nbsp;second(s)</td>
            </tr>
            <tr>
                <td></td>
                <td><input tabindex="3" value="Sync" type="submit" name="sync"></td>
            </tr>
        </table>
    </form:form>
</body>