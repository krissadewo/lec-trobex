<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${not empty status}">
    STATUS : ${status}
</c:if>
<body>
    <form:form modelAttribute="systemParameter">
        <table border="0" cellspacing="3" width="800">
            <tr>
                <td width="100">API Key</td>
                <td><form:input path="value" maxlength="200" size="100" /></td>
            </tr>
            <tr>
                <td></td>
                <td><input tabindex="3" value="Save" type="submit" name="save"></td>
            </tr>
        </table>
    </form:form>
</body>