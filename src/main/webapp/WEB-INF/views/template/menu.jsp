<%-- 
    Document   : navigation
    Created on : Dec 13, 2012, 1:06:29 PM
    Author     : Cosa
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style type="text/css">
            .wfm { width:500px }
            ul { list-style:none; }
            li { padding-top:10px }
            ul, li, div { float:left }
            ul, li { width:100% }
            .expand { width:15px;height:15px; }
            .collapse { width:15px;height:15px;display:none }
        </style>
    </head>
    <body>
        <div class="wfm">
            <ul>
                <li>
                    <div>
                        <img alt="" class="expand" src="<c:url value="/resources/images/Minus.png" />" />
                        <img alt="" class="collapse" src="<c:url value="/resources/images/Plus.png" />" />
                    </div>
                    <div>&nbsp;In House Guest
                    </div>
                    <ul>
                        <li>
                            <div><a href="<c:url value="/inhouseguest"/>">In House Guest</a>
                            </div>
                        </li>
                        <li>
                            <div><a href="<c:url value="/room"/>">Room</a>
                            </div>
                        </li>
                        <li>
                            <div><a href="<c:url value="/inhouseguestbyroom"/>">In House Guest By Room</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul>
                <li>
                    <div>
                        <img alt="" class="expand" src="<c:url value="/resources/images/Minus.png" />" />
                        <img alt="" class="collapse" src="<c:url value="/resources/images/Plus.png" />" />
                    </div>
                    <div>&nbsp;Room Service
                    </div>
                    <ul>
                        <li>
                            <div><a href="<c:url value="/purchaseItem"/>">Purchase Item</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul>
                <li>
                    <div>
                        <img alt="" class="expand" src="<c:url value="/resources/images/Minus.png" />" />
                        <img alt="" class="collapse" src="<c:url value="/resources/images/Plus.png" />" />
                    </div>
                    <div>&nbsp;Post Billings
                    </div>
                    <ul>
                        <li>
                            <div><a href="<c:url value="/postUsageAdmission"/>">Post Usage Admission</a>
                            </div>
                        </li>
                        <li>
                            <div><a href="<c:url value="/postRestaurant"/>">Post Restaurant</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul>
                <li>
                    <div>
                        <img alt="" class="expand" src="<c:url value="/resources/images/Minus.png" />" />
                        <img alt="" class="collapse" src="<c:url value="/resources/images/Plus.png" />" />
                    </div>
                    <div>&nbsp;Settings
                    </div>
                    <ul>
                        <li>
                            <div><a href="<c:url value="/settingApiKey"/>">API Key</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <script type="text/javascript" language="javascript">
            $(".expand").click(function () {
                $(this).toggle();
                $(this).next().toggle();
                $(this).parent().parent().children().last().toggle();
            });
            $(".collapse").click(function () {
                $(this).toggle();
                $(this).prev().toggle();
                $(this).parent().parent().children().last().toggle();
            });
        </script>
    </body>
</html>
