<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script language="javascript" type="text/javascript" src="<c:url value="/resources/js/jquery-click.js"/>"></script>
        <title><tiles:insertAttribute name="title" ignore="true"/></title>
    </head>
    <body>
        <table border="1" cellpadding="2" cellspacing="2" align="center">
            <tr>
                <td height="30" colspan="2" align="center"><tiles:insertAttribute name="header"/>
                </td>
            </tr>
            <tr>
                <td height="250"><tiles:insertAttribute name="menu"/></td>
                <td width="350" valign="top">
                    <table border="0" cellpadding="2" cellspacing="0">
                        <tr>
                            <td><h2><tiles:getAsString name="title"/></h2></td>
                        </tr>
                        <tr>
                            <td><tiles:insertAttribute name="content"/></td>
                        </tr>
                    </table>
                </td>
            </tr>
<!--            <tr>
                <td height="30" colspan="2"><tiles:insertAttribute name="footer" />
                </td>
            </tr>-->
        </table>
    </body>
</html>